//
//  SchoolDetailViewModel.swift
//  NYC_APP
//
//  Created by Shaik Mohaideen A S on 3/14/23.
//

import Foundation



/// Model For to display Details in School Detail Screen
struct SchoolDetailModel {
    var schoolName: String?
    var schoolEmail: String?
    var website: String?
    var primaryAddressLine: String?
    var city: String?
    var zip: String?
    var latitude: String?
    var longitude: String?
    var math: String?
    var reading: String?
    var writing: String?
}


struct SchoolDetailViewModel {
    
    /// Method to get Model Mapped from diffrent response
    /// - Parameters:
    ///   - detailResponse: NYCSchoolListModel
    ///   - csatDetail: NYCSchoolDetailsModel
    /// - Returns: SchoolDetailModel
    func mapModelFromResponse (_ detailResponse : NYCSchoolListModel, _  csatDetail: NYCSchoolDetailsModel) -> SchoolDetailModel{
        
        var detailModel = SchoolDetailModel()
        detailModel.schoolName = detailResponse.schoolName
        detailModel.schoolEmail = detailResponse.schoolEmail
        detailModel.website = detailResponse.website
        detailModel.primaryAddressLine = detailResponse.primaryAddressLine
        detailModel.zip = detailResponse.zip
        detailModel.latitude = detailResponse.latitude
        detailModel.longitude = detailResponse.longitude
        detailModel.math = csatDetail.satMathAvgScore
        detailModel.reading = csatDetail.satCriticalReadingAvgScore
        detailModel.writing = csatDetail.satWritingAvgScore
        return detailModel
        
    }
    
    /// Function to get info for Detail screen
    /// - Parameters:
    ///   - schoolId: String
    ///   - detailList: [NYCSchoolDetailsModel]
    /// - Returns: [NYCSchoolDetailsModel]
    func getDetailModelwithId (_ schoolId: String, detailList : [NYCSchoolDetailsModel]) -> [NYCSchoolDetailsModel]? {
        return detailList.filter{ $0.dbn == schoolId }
    }
    
}
