//
//  NycSchoolModel.swift
//  NYC_APP
//
//  Created by Shaik Mohaideen A S on 3/14/23.
//

import Foundation

struct NYCSchoolDetailsModel: Codable {
    private enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
        
    }
    let dbn: String
    let schoolName: String
    let numOfSatTestTakers: String
    let satCriticalReadingAvgScore: String
    let satMathAvgScore: String
    let satWritingAvgScore: String
}


struct NYCSchoolListModel: Codable {
    private enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case academicopportunitiesOne = "academicopportunities1"
        case academicopportunitiesTwo = "academicopportunities2"
        case phoneNumber = "phone_number"
        case faxNumber = "fax_number"
        case schoolEmail = "school_email"
        case website = "website"
        case finalgrades
        case totalStudents = "total_students"
        case primaryAddressLine = "primary_address_line_1"
        case city = "city"
        case zip = "zip"
        case stateCode = "state_code"
        case borough = "borough"
        case latitude
        case longitude
    }
    let dbn: String
    let schoolName: String
    let overviewParagraph: String
    let academicopportunitiesOne: String?
    let academicopportunitiesTwo: String?
    let phoneNumber: String
    let faxNumber: String?
    let schoolEmail: String?
    let website: String
    let finalgrades: String
    let totalStudents: String
    let primaryAddressLine: String
    let city: String
    let zip: String
    let stateCode: String
    let borough: String?
    let latitude: String?
    let longitude: String?
}
