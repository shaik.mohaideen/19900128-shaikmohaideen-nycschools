//
//  ResponseManager.swift
//  NetworkManager
//
//  Created by Shaik Mohaideen A S on 3/14/23..
//

import Foundation
/// ResponseManager is only responsible Maping the response to codable class SOLID - Followed Single Principle
class ResponseManager {
    
    class func responseMapper<T: Codable> (data : Data, responseClass : T.Type, completion: @escaping (Bool, T?) -> ()) {
        
        let jsonDecoder = JSONDecoder()
        if let decodeResponse = try? jsonDecoder.decode(responseClass.self, from: data)
        {
            return completion(true, decodeResponse.self)
        }else {
            return completion(false, nil)
        }
        
    }
    
}




