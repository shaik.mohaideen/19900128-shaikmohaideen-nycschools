//
//  DetailViewController.swift
//  NYC_APP
//
//  Created by Shaik Mohaideen A S on 3/14/23.
//

import UIKit
import MapKit

class DetailViewController: UIViewController {
    @IBOutlet weak var schoolMapView: MKMapView!
    @IBOutlet weak var schoolNameLbl: UILabel!
    @IBOutlet weak var schoolAddressLbl: UILabel!
    @IBOutlet weak var schoolZipLbl: UILabel!
    @IBOutlet weak var mathLbl: UILabel!
    @IBOutlet weak var readingLbl: UILabel!
    @IBOutlet weak var writingLbl: UILabel!
    @IBOutlet weak var schoolEmailLbl: UITextView!
    @IBOutlet weak var schoolLinTextView: UITextView!
    var schoolDetailViewModel = SchoolDetailModel()
    
    override func viewDidLoad() {
        let annotation = MKPointAnnotation()
        if let lat = Double(schoolDetailViewModel.latitude ?? ""), let long = Double(schoolDetailViewModel.longitude ?? "")
        {
            annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            schoolMapView.addAnnotation(annotation)
        }
        schoolNameLbl.text = schoolDetailViewModel.schoolName
        schoolAddressLbl.text = schoolDetailViewModel.primaryAddressLine
        schoolZipLbl.text = schoolDetailViewModel.zip
        mathLbl.text = schoolDetailViewModel.math
        readingLbl.text = schoolDetailViewModel.reading
        writingLbl.text = schoolDetailViewModel.writing
        schoolEmailLbl.text = schoolDetailViewModel.schoolEmail
        schoolLinTextView.text = schoolDetailViewModel.website
    }

}
