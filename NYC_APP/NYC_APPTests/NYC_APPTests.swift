//
//  NYC_APPTests.swift
//  NYC_APPTests
//
//  Created by Shaik Mohaideen A S on 3/14/23.
//

import XCTest
@testable import NYC_APP

final class NYC_APPTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    /// Method to find Value is not availeble in the array
    func testNoValue() throws {
        let schoolDetailViewModel = SchoolDetailViewModel()
        let result = schoolDetailViewModel.getDetailModelwithId("xxx", detailList: [NYCSchoolDetailsModel]())
        XCTAssertEqual(result?.count, 0)
       
    }
    

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
